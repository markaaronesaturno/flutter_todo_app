import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/screens/register_screen.dart';
import '/screens/login_screen.dart';
import '/screens/task_list_screen.dart';
import 'providers/user_provider.dart';

Future<void> main() async {
    //initial checks for user access token from sharedPreferences.
    //Determine initial route of app depending on existence of users access token
    //ensures all widgets are ready
    WidgetsFlutterBinding.ensureInitialized();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('accessToken');
    String initialRoute = (accessToken != null) ? '/task-list' : '/';
    
    await dotenv.load(fileName: "assets/env/.env_web");
    print(dotenv.env['API_URL']);

    runApp(App(accessToken, initialRoute));
}

class App extends StatelessWidget{
    final String? _accessToken;
    final String _initialRoute;

    App(this._accessToken,this._initialRoute);
    
    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider(
            create: (BuildContext context) => UserProvider(_accessToken),
            child:MaterialApp(
                theme: ThemeData(
                    primaryColor: Color.fromRGBO(255, 212, 71, 1),
                    elevatedButtonTheme: ElevatedButtonThemeData(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(255, 212, 71, 1), //Background Color
                            onPrimary: Colors.black //Text Color
                        )
                    )
                ),
                initialRoute: _initialRoute,
                routes: {
                    '/': (context) => LoginScreen(),
                    '/register': (context) => RegisterScreen(),
                    '/task-list': (context) => TaskListScreen(),
                }
            )
        );
    }
}